default = """
# Obviously, don't share login information with anyone you don't trust with the account.
# For an explanation, see https://asyncpraw.readthedocs.io/en/stable/getting_started/configuration/prawini.html#praw-ini.
[DEFAULT]
# A boolean to indicate whether or not to check for package updates.
check_for_updates=True
# Object to kind mappings
comment_kind=t1
message_kind=t4
redditor_kind=t2
submission_kind=t3
subreddit_kind=t5
trophy_kind=t6
# The URL prefix for OAuth-related requests.
oauth_url=https://oauth.reddit.com
# The amount of seconds of ratelimit to sleep for upon encountering a specific type of 429 error.
ratelimit_seconds=5
# The URL prefix for regular requests.
reddit_url=https://www.reddit.com
# The URL prefix for short URLs.
short_url=https://redd.it
# The timeout for requests to Reddit in number of seconds
timeout=16
[bot]
bot_version=1.0.0
bot_name=poser_pastas
bot_author=Ernest Izdebski
client_id={client_id}
client_secret={client_secret}
username={username}
password={password}
user_agent=script:%(bot_name)s:v%(bot_version)s (by u/%(bot_author)s)
"""


def main() -> None:
    print(
        "Initializing login credentials. If you do not have a bot application,\n"
        "see https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example#first-steps.\n"
        "I recommend setting 'redirect url' to https://rattrivia.com :Tolololol:\n"
    )

    kwargs: dict[str, str] = {
        cred: input(f"{cred}?  ") for cred in ("client_id", "client_secret", "username", "password")
    }

    resp = default.format(**kwargs)

    try:
        with open("praw.ini", "w", encoding="utf-8") as file:
            file.write(resp)
    except FileExistsError:
        cont = input("praw.ini already exists. Continue?  ").strip()
        if cont == "" or cont[0].lower() not in ("1", "y", "t"):
            return
        with open("praw.ini", "w+", encoding="utf-8") as file:
            file.write(resp)
    finally:
        print("Wrote to praw.ini.")
        input("Press enter to continue")


if __name__ == "__main__":
    main()
