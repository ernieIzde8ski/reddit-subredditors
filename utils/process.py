import asyncio
from typing import AsyncGenerator, Generator

from asyncpraw import Reddit
from asyncpraw.models import Comment, MoreComments, Submission, Subreddit
from common import Commentor, Comments, Config, Export

from utils import file
from utils.collect import collect_submissions

comments: Comments = {}
config: Config = file.load("config.json", prepend=False)


def save(
    max_subs: int,
    max_coms: int,
    *,
    p1: str = "comments_simple.json",
    p2: str = "comments_extended.json",
    final: bool = False,
) -> None:
    print("Saving...")
    # Renders the comment in a nice, easy list of tuples.
    simple: list[tuple[str, int, int]] = []
    for commentor, items in comments.items():
        ls = len(items["submitted"])
        lc = len(items["comments"])
        if ls > max_subs or lc > max_coms:
            simple.append((commentor, ls, lc))
    if final:
        simple.sort(key=lambda i: i[2])
    # This one just replaces the set with a list.
    extended = {k: {"comments": list(v["comments"]), "submitted": list(v["submitted"])} for k, v in comments.items()}

    file.dump(p1, simple)
    file.dump(p2, extended)
    print("Saving!")


def handle_submission(submission: Submission) -> None:
    if submission.author is None:
        return
    if comments.get(submission.author.name) is None:
        print(f"New Author! u/{submission.author.name}")
        comments[submission.author.name] = Commentor(comments=set(), submitted={submission.id})
    else:
        comments[submission.author.name]["submitted"].add(submission.id)


async def handle_comment(comment: Comment | MoreComments) -> None:
    if isinstance(comment, MoreComments):
        return
    for subcomment in comment.replies:
        await handle_comment(subcomment)
    if comment.author is None:
        return
    _id = comment.id
    if comments.get(comment.author.name) is None:
        print(f"New Author! u/{comment.author.name}")
        comments[comment.author.name] = Commentor(comments={_id}, submitted=set())
    elif _id in comments[comment.author.name]["comments"]:
        return
    else:
        comments[comment.author.name]["comments"].add(_id)


@Export("Process")
async def process_subreddit(reddit: Reddit, subreddit: Subreddit, *, ids: set[str] | None = None) -> None:
    if ids is None:
        try:
            ids = set(file.load("submission_ids.json"))
        except FileNotFoundError:
            print("Submission IDs file not found! Running collect_submissions in 5 seconds...")
            await asyncio.sleep(5)
            ids = set(await collect_submissions(reddit, subreddit, ids=set()))
    
    for l, id in enumerate(ids):
        submission = await reddit.submission(id)
        handle_submission(submission)
        for comment in submission.comments:
            await handle_comment(comment)
        if l % 10 == 0:
            save(config["maxSubmissionsAllowed"], config["maxCommentsAllowed"])
    save(config["maxSubmissionsAllowed"], config["maxCommentsAllowed"], final=True)
