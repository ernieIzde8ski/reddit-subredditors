from random import choice

from asyncpraw import Reddit
from asyncpraw.exceptions import RedditAPIException
from asyncpraw.models import Redditor, Subreddit
from common import Config, Export

from utils import file

ban_reason_base = """
You have been permanently banned from participating in r/{subreddit}. You can still view and subscribe to r/{subreddit}, but you won't be able to post or comment. For more information, see {link}

Note from the moderators:

{reason}
""".strip()


def get_redditors(path: str) -> list[str]:
    print(path)
    try:
        return [i[0] for i in file.load(path)]
    except (TypeError, IndexError):
        raise ValueError(f"{path} is not formatted correctly! (Correct: list[list[str, int, int]])")


async def get_banned_users(subreddit: Subreddit, *, limit: int = 15) -> list[str]:
    resp = []
    ban: Redditor | None
    async for ban in subreddit.banned():
        if ban is None:
            continue
        else:
            resp.append(ban.name)
    return resp


def generate_ban_reasons(subreddit: Subreddit, *config_reasons: str) -> tuple[str, list[str]]:
    """Generate reasons used in banning"""
    r1 = f"You have been permanently banned from participating in r/{subreddit.display_name}"
    link = input("Link?") or "https://reddit.com"
    r2 = [
        ban_reason_base.format(subreddit=subreddit.display_name, reason=reason, link=link) for reason in config_reasons
    ]
    return r1, r2


def warn(total: int) -> None:
    print(f"Are you *absolutely sure* you want to ban {total} users?")
    print("Press ctrl+c to raise a KeyboardInterrupt error instead.")
    input(), input()


@Export("Ban")
async def ban_collected_users(
    reddit: Reddit, subreddit: Subreddit, *, redditors: list[str] | None = None, path: str = "comments_simple.json"
) -> None:
    config: Config = file.load("config.json", prepend=False)
    if redditors is None:
        redditors = get_redditors(path)

    banned_users = await get_banned_users(subreddit)
    print(f"Already banned users: {banned_users[:15] + (banned_users[15:] and ['...'])}")

    victims = list(filter(lambda r: r not in config["skips"] and r not in banned_users, redditors))
    title, ban_reasons = generate_ban_reasons(subreddit, *config["banReasons"])

    warn(len(victims))

    for victim in victims:
        try:
            await subreddit.banned.add(victim, ban_reason=config["banReason"])
            await subreddit.modmail.create(title, choice(ban_reasons), victim, author_hidden=True)
            print(f"Banned redditor u/{victim}")
        except RedditAPIException as e:
            for i in e.items:
                if i.error_type == "CANT_RESTRICT_MODERATOR":
                    print(f"Attempted to ban u/{victim}, but u/{victim} is a moderator.")
                    continue
                raise e
