import json
from typing import Any, Callable


def _prepend_path(path: str, *, _prepend: str = "data") -> str:
    return f"{_prepend}/{path}"


def load(path: str, default: Callable[..., Any] | None = None, *args, prepend: bool = True, **kwargs) -> Any:
    """Loads a JSON file.
    If a default is supplied, and the file is not found, returns the default."""
    if prepend:
        path = _prepend_path(path)
    try:
        with open(path, mode="r", encoding="utf-8") as file:
            return json.load(file)
    except FileNotFoundError as err:
        if default is None:
            raise err from err
        resp = default(*args, **kwargs)
        with open(path, mode="w", encoding="utf-8") as file:
            json.dump(resp, file)
        return resp


def dump(path: str, obj: Any, *, prepend: bool = True) -> None:
    if prepend:
        path = _prepend_path(path)
    if isinstance(obj, set):
        obj = list(obj)
    with open(path, mode="w+", encoding="utf-8") as file:
        json.dump(obj, file)
