import asyncio
import re

from asyncpraw import Reddit
from asyncpraw.models import ListingGenerator, Submission, Subreddit
from common import Export, SubmissionIDs, now


def save(ids: SubmissionIDs) -> None:
    print(f"{now()} | Saving...")
    ids.save()
    print(f"{now()} | Saved!")


@Export("Collect (Stream)")
async def collect_submissions_forever(reddit: Reddit, subreddit: Subreddit) -> None:
    """Collects submission IDs through a stream and dumps them into submission_ids.json.

    Should never terminate.
    """
    ids = SubmissionIDs.load()
    submission: Submission
    index = 0
    async for submission in subreddit.stream.submissions():
        sr_name: str = submission.subreddit.display_name  # typehinting
        if submission.id in ids[sr_name]:
            continue  # skip printing of known ids
        print(f"Adding submission from r/{sr_name} with ID {submission.id}...")
        ids[sr_name].add(submission.id)
        index += 1
        if index % 25 == 0:
            save(ids)


SubredditPrefix = re.compile(r"r/[^\/]*/")
EmptyReplacement = ""


async def process_submissions_generator(generator: ListingGenerator, ids: SubmissionIDs) -> None:
    """Handle each submission in generator"""
    submission: Submission
    async for submission in generator:
        sr_name: str = submission.subreddit.display_name  # typehinting
        if submission.id in ids[sr_name]:
            continue  # skip printing of known ids
        print(f"Adding submission from r/{sr_name} with ID {submission.id}...")
        ids[sr_name].add(submission.id)
    url = re.sub(SubredditPrefix, EmptyReplacement, generator.url)
    print(f"{now()} | Done collecting from generator '{url}'!")


@Export("Collect")
async def collect_submissions(
    reddit: Reddit, subreddit: Subreddit, *, ids: SubmissionIDs | None = None
) -> SubmissionIDs:
    """Collects submission IDs and dumps them into submission_ids.json"""
    if ids is None:
        ids = SubmissionIDs.load()

    subreddit.new()
    methods = subreddit.new, subreddit.hot, subreddit.top, subreddit.rising
    generators = (process_submissions_generator(m(limit=None), ids) for m in methods)  # type: ignore
    await asyncio.gather(*generators)
    save(ids)
    return ids
