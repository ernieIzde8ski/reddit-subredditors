import asyncio
from collections import defaultdict
from contextlib import suppress
from functools import cache
from typing import Any, Callable, NoReturn

from asyncpraw import Reddit
from asyncpraw.models import Subreddit

# ensures functions are @Export'ed
from utils import collect, process, ban
from common import Export, ExportedFunctions, get_command_line_arguments


@Export("Exit")
async def _exit(*args, **kwargs) -> NoReturn:
    raise KeyboardInterrupt


@Export("Retry")
async def retry(*args, **kwargs) -> None:
    pass


def get_function(__default=None, /) -> Callable[..., Any]:
    for i, foo in ExportedFunctions:
        print(f"[{i}] {foo.name}") # type: ignore

    opt = (__default or "").strip() or input("Action? ").strip()
    try:
        return ExportedFunctions[int(opt)][1]
    except (IndexError, ValueError):
        print("Invalid input!")
        return retry


async def main() -> None:
    kwargs = get_command_line_arguments()

    default = "Sabaton"
    print(kwargs)
    sub_input = kwargs["-sr"] or input(f"Which subreddit ({default=})? ").strip() or default

    async with Reddit("bot", config_interpolation="basic") as reddit:
        subreddit: Subreddit = await reddit.subreddit(sub_input)
        while True:
            func = get_function(kwargs["-opt"])
            print(f"Selected function {func.__name__}")
            print()
            await func(reddit, subreddit)
            if (kwargs["-sr"] and kwargs["-opt"]):
                break
            sub_input = input(f"Which subreddit ({default=})? ") or default
            if sub_input.lower() != subreddit.display_name.lower():
                subreddit = await reddit.subreddit(sub_input)


if __name__ == "__main__":
    with suppress(KeyboardInterrupt):
        asyncio.get_event_loop().run_until_complete(main())
    print()
    print("Goodbye!")