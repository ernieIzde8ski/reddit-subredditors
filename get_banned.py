import re
from asyncio import get_event_loop

from asyncpraw import Reddit
from asyncpraw.models import Subreddit

from utils import file


pattern = re.compile(r".+ \(.+ incident\)\.")


async def main(*, default="MetalMemes", **kwargs):
    async with Reddit("bot", config_interpolation="basic") as reddit:
        sub: Subreddit = await reddit.subreddit(input(f"Subreddit? ({default=}) ") or default)
        path = f"banned_{sub.display_name}.json"
        prev = file.load(path, set)
        resp = await fetch_banned(sub, **kwargs)
        file.dump(path, resp)
        display_stats(prev, resp)


async def fetch_banned(sub: Subreddit, *, debug: bool, lim=999999) -> set[str]:
    resp = set()
    async for ban in sub.banned(limit=lim):
        if re.match(pattern, ban.note):
            if debug:
                print(ban.note)
            resp.add(ban.name)
        elif debug:
            print(f"Skipping user {ban.name}: Invalid ban reason")
    return resp


def display_stats(prev: set[str], now: set[str]) -> None:
    print(f"New bans: {sorted([i for i in now if i not in prev])}")
    print(f"New unbans: {sorted([i for i in prev if i not in now])}")


if __name__ == "__main__":
    get_event_loop().run_until_complete(main(debug=False))
