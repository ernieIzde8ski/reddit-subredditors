# Reddit Subredditors

Reddit Subredditors attempts to collect as many Redditors as possible from a subreddit, and enables the mass banning of these redditors. Originally created for the Christmas 2021 Incident of [r/MetalMemes](https://reddit.com/r/MetalMemes), in which 739 users from r/Sabaton were banned in the first wave.

## Setup

- Python 3.10

Run:

```bash
python -m pip install -r requirements.txt
python setup.py
```

## Usage

`venv` usage is advised. Run `python index.py` for the following sub-actions.

Command-line arguments can also be passed to automate it: `python index.py --opt 0 --sr Sabaton` will collect continuously from `r/Sabaton`. Note that 3 and 5 are exceptions - the former always requires additional input, and the latter will not work as intended.

### Collection [1]

Collects submission IDs to be used in Processing. Terminates once complete.

#### Collection (Streams) [0]

Collects submission IDs with a stream. Does not terminate.

### Processing [2]

Processes each saved submission ID & saves all data to comments_extended.json, filtered data to comments_simple.json. If submission_ids.json does not exist, executes Collection first.

### Banning [3]

Bans each user found in comments_simple.json. Takes into account the config file and already banned users before/while banning.
<!--TODO: Will not send secondary messages to any user inactive in r/MM. -->

### Exit [4]

Exits.

### Retry [5]

Reprompts for subreddit, action.
