from collections import defaultdict
from contextlib import suppress
from datetime import datetime
from functools import cache
import json
from typing import Any, Callable, Literal, TypedDict, TypeVar

from utils import file

TimeSpec = Literal["auto", "hours", "minutes", "seconds", "milliseconds", "microseconds"]


def now(timespec: TimeSpec = "seconds"):
    return datetime.now().isoformat(sep=" ", timespec=timespec)


class Config(TypedDict):
    maxCommentsAllowed: int
    """Maximum comments allowed before being appended to comments_simple.json"""
    maxSubmissionsAllowed: int
    """Maximum submissions allowed before being appended to comments_simple.json"""
    banReason: str
    """Reason used in mod actions while banning collecting users"""
    banReasons: list[str]
    """Reasons given while banning collected users"""
    skips: list[str]
    """Users to skip while banning collected users"""


class Commentor(TypedDict):
    """A set per an author's comments' IDs and submissions' IDs."""

    comments: set[str]
    submitted: set[str]


Comments = dict[str, Commentor]

F = TypeVar("F")
ExportedFunctions: list[tuple[int, Callable]] = []


def Export(name: str) -> Callable[[Callable], Callable]:
    def outer(func: Callable) -> Callable:
        setattr(func, "name", name)
        ExportedFunctions.append((len(ExportedFunctions), func))
        return func

    return outer


@cache
def get_command_line_arguments() -> defaultdict[str, str | Any]:
    import sys

    token = ""
    resp:  defaultdict[str, str | Any] = defaultdict(str)
    for arg in sys.argv[1:]:
        if arg.startswith("-"):
            token = arg[1:].strip()
        else:
            resp[token] += f"{arg} "
    for token, arg in resp.items():
        arg = resp[token].strip()
        with suppress(json.JSONDecodeError):
            arg = json.loads(arg)
        resp[token] = arg
    return resp


### The data/ folder:
class SubmissionIDs(defaultdict[str, set[str]]):
    """Mapping of [subreddit name, submission ids from subreddit]"""

    path = "data/submission_ids.json"

    @classmethod
    def load(cls, path: str | None = None):
        """Classmethod to create a SubmissionIDs instance from path."""
        kwargs: dict[str, list[str]] = file.load(path=path or cls.path, default=dict, prepend=False)
        return cls(set, **{kw: set(arg) for kw, arg in kwargs.items()})

    def save(self, path: str | None = None):
        path = path or self.path
        obj = {kw: list(arg) for kw, arg in self.items()}
        file.dump(path, obj, prepend=False)
